module Main where

import Control.Concurrent.Async
import Control.Monad
import Control.Monad.Trans.Maybe
import Data.Foldable
import Data.List
import Data.List.Index
import System.Exit
import System.Process

data Machine = Machine { host :: String, through :: [Machine], user :: String} deriving (Eq, Show, Read)

parse_machine :: String -> Machine
parse_machine = read

is_online :: Machine -> IO Bool
is_online machine = fmap (all (\(e, _, _) -> ExitSuccess == e)) $ traverse (\h -> readProcessWithExitCode "ping" ["-c 1", "-w 1", h] "") $ host machine : (fmap host $ through machine)

on :: String -> Machine -> MaybeT IO (String, String)
on command machine = MaybeT $ do
  (resc, out, err) <- readProcessWithExitCode "ssh" [user machine <> "@" <> host machine, command] ""
  if resc == ExitSuccess
    then return $ Just (out, err)
    else return Nothing

first_machine :: String -> [Machine] -> MaybeT IO (String, String)
first_machine command machines = msum $ fmap (on command) machines

main :: IO ()
main = do
  machines <- fmap (fmap parse_machine . lines) $ readFile "machines.lst"
  commands <- fmap lines getContents
  results <- mapConcurrently (\(c,ms) -> runMaybeT (first_machine c ms)) $ zip commands $ imap drop $ repeat $ cycle machines
  traverse_ print results
